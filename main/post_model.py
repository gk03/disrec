import pandas as pd 
from collections import OrderedDict
from sklearn.externals import joblib
from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.contrib.staticfiles.templatetags.staticfiles import static

def model_scan(a):
	head = ["CBC: RED BLOOD CELL COUNT","METABOLIC: AST/SGOT","CBC: NEUTROPHILS","CBC: HEMOGLOBIN","CBC: BASOPHILS","METABOLIC: CALCIUM","CBC: RDW","METABOLIC: CHLORIDE","CBC: EOSINOPHILS","METABOLIC: TOTAL PROTEIN","METABOLIC: POTASSIUM","METABOLIC: ALT/SGPT","CBC: ABSOLUTE NEUTROPHILS","CBC: MEAN CORPUSCULAR VOLUME","CBC: MONOCYTES","URINALYSIS: WHITE BLOOD CELLS","CBC: LYMPHOCYTES","CBC: HEMATOCRIT","URINALYSIS: PH","METABOLIC: SODIUM","CBC: MCHC","CBC: PLATELET COUNT","URINALYSIS: RED BLOOD CELLS","CBC: WHITE BLOOD CELL COUNT","METABOLIC: CARBON DIOXIDE","CBC: MCH","METABOLIC: CREATININE","METABOLIC: BILI TOTAL","CBC: ABSOLUTE LYMPHOCYTES","METABOLIC: ALBUMIN","METABOLIC: BUN","METABOLIC: ANION GAP","METABOLIC: ALK PHOS","METABOLIC: GLUCOSE","URINALYSIS: SPECIFIC GRAVITY","PatientRace","PatientMaritalStatus","PatientGender","PatientLanguage","PatientPopulationPercentageBelowPoverty"]

	#val = [3.3,13.6,6.3,13.3,0.1,10.7,15.8,103.5,0.4,8.3,4.2,22.5,79.9,94.7,1.0,1.5,2.6,33.8,6.6,133.1,33.9,331.9,2.7,11.8,24.6,23.3,0.8,1.0,20.7,5.3,23.5,4.8,114.1,136.8,1.0,1,3,0,1,0.79] 

	#val1 = [4.5,33.4,3.9,11.0,0.2,9.5,14.8,99.0,0.4,5.5,5.7,16.0,68.5,98.3,0.3,2.6,4.1,44.6,5.6,128.2,38.5,177.1,2.1,6.8,18.9,30.8,1.0,0.3,26.6,2.9,16.1,7.1,47.2,120.5,1.0,2,2,0,1,19.17] 

	dic = OrderedDict()
	for i in range(40):
		dic[head[i]] = a[i]

	df1 = pd.DataFrame(columns=head)
	df1 = df1.append(dic, ignore_index=True)
	import os
	MYDIR = os.path.dirname(__file__)
	filename = os.path.join(MYDIR, 'model_DecisionTreeClassifier.mg')
	classification_model = joblib.load(filename)

	pred =  classification_model.predict(df1)

	df = pd.read_csv("~/disrec/main/full_dataset.csv",sep=",",error_bad_lines=False,index_col=False)
	otn = df.loc[df["PrimaryDiagnosisCode"] == pred[0]]

	otn.PatientRace.replace(['White', 'Unknown', 'Asian', 'African American'], [1, 0,2,3], inplace=True)
	otn.PatientLanguage.replace(['English', 'Icelandic', 'Spanish', 'Unknown'], [1, 2,3,0], inplace=True)
	otn.PatientMaritalStatus.replace(['Unknown', 'Married', 'Widowed', 'Single', 'Divorced', 'Separated'], [0, 1,2,3,4,5], inplace=True)
	otn.PatientGender.replace(['Female', 'Male'], [0,1], inplace=True)

	otn = otn.loc[otn["PatientRace"] == dic["PatientRace"]]
	otn = otn.loc[otn["PatientLanguage"] == dic["PatientLanguage"]]
	#otn = otn.loc[otn["PatientMaritalStatus"] == dic["PatientMaritalStatus"]]
	otn = otn.loc[otn["PatientGender"] == dic["PatientGender"]]

	pid_index = {}
	for i,j in otn.iterrows():
		pid_index[j["pid"]] = j["AdmissionStartDate"]

	dis_index = {}
	for i in pid_index:
		otn1 = df.loc[df["pid"] == i]
		for j,k in otn1.iterrows():
			if k["PrimaryDiagnosisCode"] != pred[0]: 
				end = k["AdmissionStartDate"].split()[0]
				end  = datetime.strptime(end,"%Y-%m-%d")
				date_srt = pid_index[i].split()[0]
				date_srt = datetime.strptime(date_srt,"%Y-%m-%d")
				rdelta = relativedelta(end, date_srt)
				if(rdelta.years > -1):
					if rdelta.years in dis_index:
						tmp = dis_index[rdelta.years]
						tmp.append(k["PrimaryDiagnosisCode"])
						dis_index[rdelta.years] = tmp
					else:
						dis_index[rdelta.years] = [k["PrimaryDiagnosisCode"]]
	
	dis_index["predicted"] = pred[0]
	return dis_index

	
