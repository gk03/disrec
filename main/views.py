from django.shortcuts import render
from django.http import HttpResponse
from sklearn.externals import joblib
from post_model import model_scan
from django.http import JsonResponse

# Create your views here.
def index(request):
	if request.method == "POST":
		content = request.POST.get("data",[6.9,37.8,10.6,16.3,0.2,10.4,11.0,109.7,0.3,8.4,4.5,18.3,79.6,73.5,0.5,3.3,0.5,34.6,6.5,152.5,33.2,152.5,0.5,5.9,27.5,32.9,1.0,0.4,23.4,4.6,29.3,7.9,147.9,127.9,1.0,2,2,0,1,19.17]) 
		content = content.split(",")
		data = []
		for i in content:
			data.append(float(i))
		print type(data)
		print (data)
		out = model_scan(data)
		return JsonResponse(out)
	else:
		msg = {}
		msg["status"] = "202 Invalid"
		return JsonResponse(msg)


